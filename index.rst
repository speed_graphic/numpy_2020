####################################
Ideas for NumPy: Season of Docs 2020
####################################

I hope to participate in Season of Docs `(samples, bio)`_ and am thrilled to find NumPy again
among the organizations seeking help.  I'm amazed, as you surely are, by
NumPy's importance. [#hello]_

.. _`(samples, bio)`: https://docs2020.bennathanson.com

Though it's early, I'm eager to start a conversation. I have a number of thoughts about contributing.

*****************
The fourfold way
*****************

Search for advice on losing weight, saving money, or writing documentation,
and everyone has the same few things to say.  Daniele Procida
changed the game with his insight on separation of concerns [#procida]_. I could not be
happier that you plan to put those insights to use.

.. _`put those insights to use`: https://numpy.org/neps/nep-0044-restructuring-numpy-docs.html#detailed-description
.. _`insight`: https://documentation.divio.com/

****************
Views and copies
****************

For the wave of incoming technical writers, Season of Docs is foreign language
immersion.

This is true for me, too. There are two upsides in it:

- I see learning NumPy not as a hurdle to surmount but as a reward in itself: the journey is the destination.
- As an engineer, I have a foundation to build on.

I've done editing and can organize, but my hope is to write. How
about this: walk an upward gradient of doc issues, starting perhaps with
`Copies vs. Views`_, and each time, from a mentor's suggestion, picking one
that requires more knowledge. Insofar as possible, I'd learn on my own.

.. _`Copies vs. Views`: XXXXXX

What's my explanatory writing like? To add to the writing samples linked
above, I started Copies vs. Views, where I'm indebted to XXX's comments in
Issue XXXX.

*******************
Potential new pages
*******************

- A "NumPy Traps and Pitfalls" page would be widely read [#koenig]_.

- We're concerned about users picking up obsolete information from sites like
  Stack Overflow. Also, longtime users might be doing things the hard way
  because they haven't gotten the word on recent features. I haven't come up with
  a catchy title, but a page summarizing old ways and new improved ways could be
  useful. [#release_notes]_

- NumPy isn't always the right tool. Higher-level approaches may be
  better for certain tasks; there may be applications where NumPy is known
  to fall short. We could add a page on when not to use NumPy.


**********************************************************
Search, internal and external, needs a hit upside the head
**********************************************************

NumPy docs search gets sold short both by Google and the internal search box. Examples:

I'm not an SEO expert, but I could seek out ways for both internal and external search to be
improved. We want our painstakingly crafted docs to be found.

If we're successful, our results could help other open-source communities, so I'd publish them.

*********************
Killing with kindness
*********************

Too many NumPy pages are offering seemingly `equivalent help`_ to newcomers:

- `What is Numpy?`_
- `NumPy basics`_
- `Quickstart tutorial`_
- `Absolute beginners tutorial`_
- `Tutorials`_

.. _`What is Numpy?`: XXXXXXX
.. _`NumPy basics`: XXXXXXX
.. _`Quickstart tutorial`: XXXXXXX
.. _`Absolute beginners tutorial`: XXXXXXX
.. _`Tutorials`: XXXXXXX

I could improve new-visitor experience by consolidating some pages and giving more precise names to others.

.. _`equivalent help` : https://en.wikipedia.org/wiki/Buridan%27s_ass

**********************
Setting doc priorities
**********************

As the Season of Docs proposal points out, NumPy documentation has a big-tent
problem: It has many kinds of users and each wants a different subset of docs.

If NumPy's docs were complete, everyone would be served and no problem would
exist. But that isn't true and we can benefit from setting priorities.

The team is painfully aware of shortcomings in the docbase. But doing what we
feel worst about might not optimize user benefit.

Another way to set priorities is to think about particular user
communities, and a first step is identify them and define their needs.

I could work to help set guidelines -- on whatever basis we choose --
for picking new documentation efforts [#beggars]_.

**********************
Believing the Internet
**********************

Suppose we take this viewpoint: If Stack Overflow has the question, we didn't have the answer. The topic was missing, or
was too hard to find, or didn't cover what the OP wanted. [#humanity]_ [#local_search]_

I thought it would be fun to find out

#. what NumPy questions are looked up most often, and
#. how difficult it is to find the answer in our docs.

I've extracted [#so_api]_
the `100 most viewed questions`_ of the 73,315 NumPy questions listed,
though I think the fun will wear off after looking through 10. [#rss_trends]_


.. _`100 most viewed questions`: XXXXXXX


This work is what a restaurateur would call an amuse-bouche. I don't propose it as a project; it's just for fun.


******************
Graphical site map
******************

When I see an outline of page headings I keep thinking of plot-generating software.  If a table of digits can be brought to life as an image, maybe that doc tree can too. Is an image useful internally for reorganizing sections? Does it have a user-facing role? On what scale is it useful, if at all? I'd like to find out, and again it's on the list as an amuse-bouche.


I appreciate your reading this note. I hope we can collaborate this Season.


.. rubric:: Footnotes

.. [#hello] Even if NumPy were a two-developer project in Egyptology, you had me at hello.
            I very much liked the thoughtfulness of your project ideas page.

.. [#procida] He's no help on dieting or getting rich, unfortunately.

.. [#koenig] The title is from Andy Koenig's `C Traps and Pitfalls.`
             NumPy isn't C, but every tool has skeletons.

.. [#release_notes] Release notes do something similar, but the granularity and perspective is wrong. This would be
                    a standing page added to from the top. We'd create an initial page based on experience.

.. [#beggars] I realize that in open source you sometimes just need to take
              what's offered.

.. [#humanity] I'm ruling out the cynical possibility that hundreds of thousands of people were all too lazy to look.

.. [#local_search] We could also monitor onsite search to find terms that are being searched for and terms that aren't found.
                   This seems less easy: Even if search hooks are available for it, the definition of failure
                   is complicated: it can mean no results, results not clicked on, or result clicked on and departed from
                   in exasperation.

.. [#so_api] SO searches won't sort on page views and the API doesn't include them, so this was a job for Beautiful Soup.

.. [#rss_trends] I was also curious about NumPy's flux intensity in the greater Web, so I set up a Google Alerts; here's `a few days of RSS`_. And you can watch the NumPy interest taking off in `Google Trends`_ .

    .. _`a few days of RSS`: XXXXXX

    .. _`Google Trends`: https://trends.google.com/trends/explore?date=all&q=numpy
